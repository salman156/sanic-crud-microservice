import custom_logging as cst_logs
import logging
from bussineslogic import api_bp
from sanic import Sanic
from sanic.response import html
from sanic.log import LOGGING_CONFIG_DEFAULTS

logging.setLogRecordFactory(cst_logs.record_factory)

LOGGING_CONFIG_DEFAULTS["formatters"]["access"]["format"] = cst_logs.LOGGING_FORMAT

app = Sanic('CRUDApp', log_config=LOGGING_CONFIG_DEFAULTS)

app.blueprint(api_bp)


@app.get("/")
def root(request):
    return html('<!DOCTYPE html><html lang="en"><meta charset="UTF-8"><div>Hi 😎</div>')


if __name__ == '__main__':
    app.run("0.0.0.0", 8080, access_log=True)
