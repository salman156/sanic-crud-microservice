from pydantic import BaseModel
from pydantic.types import Optional
from enum import Enum
from .user import User


class UserCreationResult(BaseModel):

    class Status(Enum):
        Success = 1,
        Duplicate = 2

    status: Status
    user: Optional[User]
