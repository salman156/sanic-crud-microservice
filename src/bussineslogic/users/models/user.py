from uuid import UUID
from pydantic import BaseModel
from pydantic.types import Optional


class User(BaseModel):
    reference: Optional[UUID]
    username: str
    password_hash: Optional[str]
