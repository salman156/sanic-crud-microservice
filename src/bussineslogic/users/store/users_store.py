import aiopg
import os
from ..models.user import User
from uuid import UUID


class UsersStore:   # TODO: pass same connection pool to stores
    def __init__(self):
        self.dsn = os.environ['PG_CONNECTION_STRING']

    async def GetUsers(self):
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute("SELECT reference, username FROM users;")

                    users = []
                    async for row in cur:
                        users.append(User(reference=row[0], username=row[1]).json())

                    return users

    async def GetUserByName(self, user_name: str) -> User:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "SELECT username FROM users WHERE username = '{}';"
                        .format(user_name)
                    )
                    user = None
                    async for row in cur:
                        user = User(username=row[0])

                    return user

    async def CreateUser(self, user: User) -> UUID:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "INSERT INTO users (username, password_hash) VALUES ('{}', '{}') RETURNING reference;"
                        .format(user.username, user.password_hash)
                    )

                    ref = ""
                    async for row in cur:
                        ref = row[0]

                    return ref

    async def DeleteUser(self, user_ref: UUID) -> int:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "DELETE FROM users WHERE reference = '%s'",
                        user_ref
                    )

                    ref = ""
                    async for row in cur:
                        ref = int(row)

                    return ref
