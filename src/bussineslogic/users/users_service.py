import bcrypt
from .store.users_store import UsersStore
from .models.user_creation_result import UserCreationResult
from .models.user import User
from uuid import UUID


def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_text_password, bcrypt.gensalt())


def check_password(plain_text_password, hashed_password):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(plain_text_password, hashed_password)


users_store = UsersStore()


class UsersService():  # service layer in case we need additional logic

    async def GetUsers(self):
        return await users_store.GetUsers()

    async def GetUserByName(self, username: str) -> User:
        return await users_store.GetUserByName(username)

    async def CreateUser(self, username: str, password: str) -> UserCreationResult:
        if (await self.GetUserByName(username)):
            return UserCreationResult(status=UserCreationResult.Status.Duplicate, user=None)
        password = get_hashed_password(password)

        newuser = User(username=username, password_hash=password)
        newuser.reference = await users_store.CreateUser(newuser)
        return UserCreationResult(status=UserCreationResult.Status.Success, user=newuser)

    async def DeleteUser(self, user_ref: UUID) -> int:
        return await users_store.DeleteUser(user_ref)
