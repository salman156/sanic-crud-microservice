from sanic import Blueprint
from sanic.response import json, HTTPResponse
from sanic_ext import openapi
from .users_service import UsersService
from uuid import UUID
from .models.user_creation_result import UserCreationResult
from .models.user_create_request import UserCreateRequest

userbp = Blueprint("users", "/users")

users_service = UsersService()


@userbp.get("/")
@openapi.body(
    {},
    description="List all user",
    required=False,
)
async def GetUsers(request) -> HTTPResponse:
    return json(await users_service.GetUsers())


@userbp.get("/<username:str>")
@openapi.body(
    {"application/json": str},
    description="Get user with corresponding username",
    required=True,
)
async def GetUserByName(request, username: str) -> HTTPResponse:
    user = await users_service.GetUserByName(username)

    if (user):
        return json(user)
    else:
        return json(status=404)


@userbp.post("/")
@openapi.body(
    UserCreateRequest,
    body_argument="create_request",
    description="Create user with corresponding username and password",
    required=True,
    validate=True
)
async def CreateUser(request, create_request: UserCreateRequest) -> HTTPResponse:
    result = await users_service.CreateUser(create_request.username, create_request.password)
    if result.status == UserCreationResult.Status.Success:   # match
        return json(str(result.user.reference), status=201)
    elif result.status == UserCreationResult.Status.Duplicate:
        return json(None, status=409)


@userbp.delete("/")
@openapi.body(
    UUID,
    body_argument="user_ref",
    description="Delete user with corresponding reference",
    required=True,
    validate=True
)
async def DeleteUser(request, user_ref: UUID) -> HTTPResponse:
    return json(await users_service.DeleteUser(user_ref))
