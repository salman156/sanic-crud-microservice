from sanic.blueprints import Blueprint
from .users.users_controller import userbp
from .posts.posts_controller import postsbp

api_bp = Blueprint.group(userbp, postsbp, url_prefix="/api")
