from .store.posts_store import PostsStore
from .models.post import Post
from uuid import UUID

posts_store = PostsStore()


class PostsService():  # service layer in case we need additional logic

    async def GetPosts(self):
        return await posts_store.GetPosts()

    async def GetPost(self, post_ref) -> Post:
        return await posts_store.GetPosts(post_ref)

    async def CreatePost(self, post: Post) -> UUID:
        return await posts_store.CreatePost(post)

    async def DeletePost(self, post_ref: UUID) -> int:
        return await posts_store.DeletePost(post_ref)
