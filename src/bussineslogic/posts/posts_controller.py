from sanic import Blueprint
from sanic.response import json, HTTPResponse
from sanic_ext import openapi
from .posts_service import PostsService
from .models.create_post_request import CreatePostRequest
from uuid import UUID

postsbp = Blueprint("posts", "/posts")

posts_service = PostsService()


@postsbp.get("/")
@openapi.body(
    {},
    description="List all posts",
    required=False,
    validate=True,
)
async def GetPosts(request) -> HTTPResponse:
    return json(await posts_service.GetPosts())


@postsbp.get("/<post_ref:uuid>")
@openapi.body(
    {"application/json": UUID},
    description="Get post with corresponding reference",
    required=True,
    validate=True,
)
async def GetPost(request, post_ref: UUID) -> HTTPResponse:
    return json(await posts_service.GetPost(post_ref))


@postsbp.post("/")
@openapi.body(
    {"application/json": CreatePostRequest},
    description="Create post with corresponding topic, author and text",
    required=True,
    validate=True,
)
async def CreatePost(request, post: CreatePostRequest) -> HTTPResponse:
    return json(await posts_service.CreatePost(post), status=201)


@postsbp.delete("/")
@openapi.body(
    {"application/json": UUID},
    description="Delete post with corresponding reference",
    required=True,
    validate=True,
)
async def DeletePost(request, post_ref: UUID) -> HTTPResponse:
    return json(await posts_service.DeletePost(post_ref))
