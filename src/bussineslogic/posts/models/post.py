from uuid import UUID
from pydantic import BaseModel


class Post(BaseModel):
    reference: UUID
    topic: str
    user_id: UUID
    fulltext: str
