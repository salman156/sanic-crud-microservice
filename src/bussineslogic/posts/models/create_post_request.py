from pydantic import BaseModel
from uuid import UUID


class CreatePostRequest(BaseModel):
    topic: str
    user_id: UUID
    fulltext: str
