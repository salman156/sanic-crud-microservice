import aiopg
import os
from ..models.post import Post
from uuid import UUID


class PostsStore:  # TODO: pass same connection pool to stores

    def __init__(self):
        self.dsn = os.environ['PG_CONNECTION_STRING']

    async def GetPosts(self):
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute("SELECT * FROM posts;")

                    posts = []
                    async for row in cur:
                        posts.append(Post(reference=row[0], topic=row[1], user_id=row[2], fulltext=row[3]))

                    return posts

    async def GetPost(self, post_ref: UUID) -> Post:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "SELECT * FROM posts WHERE reference = '%s';",
                        post_ref
                    )

                    post = Post()
                    async for row in cur:
                        post = Post(reference=row[0], topic=row[1], user_id=row[2], fulltext=row[3])

                    return post

    async def CreatePost(self, post: Post) -> UUID:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "INSERT INTO posts (topic, user_id, fulltext ) VALUES (%s, %s, %s, %s) RETURNING reference",
                        post.topic,
                        post.user_id,
                        post.fulltext
                    )

                    ref = ""
                    async for row in cur:
                        ref = UUID(row)

                    return ref

    async def DeletePost(self, post_ref: UUID) -> int:
        async with aiopg.create_pool(self.dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        "DELETE FROM posts WHERE reference = '%s'",
                        post_ref
                    )

                    ref = ""
                    async for row in cur:
                        ref = int(row)

                    return ref
