CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    reference uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
    username varchar(64) NOT NULL,
    password_hash varchar(512) NOT NULL
);

CREATE TABLE posts (
    reference uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
    topic varchar(64) NOT NULL,
    user_id uuid references users(reference) NOT NULL,
    fulltext varchar(256) NOT NULL
);
